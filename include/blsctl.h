/* blsctl.h
 *
 * Copyright 2019 Alberto Ruiz <aruiz@gnome.org>
 *
 * This file is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 3 of the
 * License, or (at your option) any later version.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

#include <stddef.h>

typedef char bls_bool; // Redefines char as a boolean

/**
 * CmdlineParam:
 *
 * Represents a set of values associated to a given kernel command line parameter,
 * A %NULL value in it represents an instance of a parameter in the kernel
 * cmdline with no value set.
 *
 * For example, a kernel cmdline such as "foo=3 foo foo=2" would have a
 * #CmdlineParam for "foo" with the following values: {"3", NULL, "2"}
 */
typedef struct {} BLSCmdlineParam;

/*
 * CmdlineParamCallback:
 * @param_value: A value of the value list of the #CmdlineParam
 * @user_data: A pointer to a private structure set by the caller of
 *             #bls_cmdline_param_foreach
 *
 * A callback to perform an operation on each value of a #CmdlineParam,
 * when @param_value is %NULL it means the parameter has an instance where
 * the value was not set.
 *
 */
typedef void (*BLSCmdlineParamCallback) (const char* param_value, void* user_data);

/**
 * bls_cmdline_param_length:
 *
 * @self: A set of values for a kernel cmdline parameter
 *
 * Returns: the amount of values for a specific parameter
 */
size_t bls_cmdline_param_length  (BLSCmdlineParam* self);

/**
 * bls_cmdline_param_foreach:
 *
 * @self: A set of values for a kernel cmdline parameter
 * @callback: The callback to be executed on each parameter value
 * @user_data: A pointer to data owned by the caller to be passed to the callback
 *
 * This function iterates over the internal list of values for the #CmdlineParam
 * and executes @callback on each one of them.
 *
 */
void   bls_cmdline_param_foreach (BLSCmdlineParam* self, BLSCmdlineParamCallback callback, void* user_data);

/**
 * bls_cmdline_param_destroy:
 *
 * @self: A set of values for a kernel cmdline parameter
 *
 * Destroys the @self
 */
void   bls_cmdline_param_destroy (BLSCmdlineParam* slef);

/* BLS Entry Operations */

/**
 * BLSEntry:
 * Represents a BLS .conf entry in the BLS directory
 */
typedef struct {} BLSEntry;

/**
 * BLSEntryList:
 * Opaque structure representing a list of #BLSEntry
 */
typedef struct {} BLSEntryList;

/**
 * bls_entry_new:
 *
 * @entry: A string that can be an index integer, the name of the entry
 *         (with or without .conf extension), the full entry path  or the
 *         kernel version string (if only one BLS entry has that kernel)
 *
 * Returns: A #BLSEntry object representing @entry, caller is responsible for
 *          calling #bls_entry_destroy to dispose it.
 */
BLSEntry*    bls_entry_new     (char* entry);

/**
 * bls_entry_destroy:
 *
 * @self: A #BLSEntry instance
 *
 * Disposes all resources used by @self
 */
void         bls_entry_destroy (BLSEntry* self);

/**
 * bls_entry_get:
 *
 * @self: A #BLSEntry instance
 * @command: The command to get the arguments for
 *
 * Gets the arguments for the BLS command @command
 *
 * Returns: 0 on failure and 1 on success
 */
char*        bls_entry_get    (BLSEntry* self, const char* command);

/**
 * bls_entry_get:
 *
 * @self: A #BLSEntry instance
 * @command: The command to set @args to
 * @args: A string reprenseting the arguments to set to @command
 *
 * Sets the arguments in @args to the BLS command @command
 *
 * Returns: Whether the call was succesful or not
 */
bls_bool     bls_entry_set    (BLSEntry* self, const char* command, const char* args);

/**
 * bls_entry_get:
 *
 * @self: A #BLSEntry instance
 * @command: The command to remove from @self
 *
 * Removes a specific @command from @self
 *
 * Returns: Whether the call was succesful or not
 */
bls_bool     bls_entry_remove (BLSEntry* self, const char* command);

/**
 * bls_entry_list:
 *
 * Retrieves a #BLSEntryList that represents all the BLS entries in the host
 *
 * Returns: A #BLSEntryList instance, the caller is responisble for calling
 *          #bls_entry_list_destroy to dispose the returned value.
 */
BLSEntryList* bls_entry_list                ();

/**
 * bls_entry_list_destroy:
 *
 * @self: The #BLSEntryList to be disposed
 *
 * Disposes memory and resources held by @self
 */
void          bls_entry_list_destroy        (BLSEntryList* self);

/**
 * CmdlineHandler:
 *
 * Opaque structure that represents a fat pointer to a #CmdlineHandler vtable
 * and the object that implements it trait/interface. This trait is implemented
 * by #BLSEntry and #BLSEntryList. See *_to_cmdline_handler methods to
 * cast those three types.
 *
 * Do not use this after the object that it was casted from has been destroyed
 */
typedef struct {
  void* ___d; // private data
  void* ___v; // private data
} BLSCmdline;

/**
 * bls_entry_to_cmdline:
 *
 * @env: The #BLSEntry instance to be casted to a #Cmdline
 *
 * Creates a #Cmdline instance
 */
BLSCmdline bls_entry_to_cmdline      (BLSEntry* entry);

/**
 * bls_entry_list_to_cmdline:
 *
 * @env: The #BLSEntryList instance to be casted to a #Cmdline
 *
 * Creates a #Cmdline instance, do not use after
 */
BLSCmdline bls_entry_list_to_cmdline (BLSEntryList* entry_list);

/**
 * bls_env_cmdline_get:
 *
 * @self: The #CmdlineHandler object
 * @kparam: The kernel commandline parameter name to get
 *
 * Gets the list of values for parameter @kparam as a #CmdlineParam
 *
 * Returns: A #CmdlineParam owned by the caller, it holds all the values
 *          for @kparam or %NULL if the parameter is not present or an error
 *          ocurred. The caller must use #bls_cmdline_param_destroy to dispose
 *          the returned #CmdlineParam.
 */
BLSCmdlineParam* bls_cmdline_get    (BLSCmdline self, const char* kparam);

/**
 * bls_cmdline_set:
 *
 * @self: The #CmdlineHandler object
 * @kparams: A %NULL terminated list of strings representing the parameters
 *           to be set.
 *
 * Sets the list of parameters in @kparams in the kernel cmdline.
 *
 * This is an example on how to use it:
 * |[<!-- language="C" -->
 * const char* params[] = {"param=value1", "param", "param2=value2", 0};
 * int ret = bls_env_cmdline_set(self, params);
 * ]|
 *
 * Returns: Whether the call was succesful or not
 */
bls_bool      bls_cmdline_set    (BLSCmdline self, const char* kparams[]);

/**
 * bls_cmdline_add:
 *
 * @self: The #CmdlineHandler object
 * @kparams: A %NULL terminated list of strings representing the parameters
 *           to be added.
 *
 * Adds the parameters listed in @kparam, this prevents replacing existing
 * paramter assignments. For example in a cmdline "param=2" we can add "param=3",
 * and it would keep the two assigments resulting in "param=2 paran=3"
 *
 * Returns: Whether the call was succesful or not
 */
bls_bool      bls_cmdline_add    (BLSCmdline self, const char* kparams[]);

/**
 * bls_cmdline_remove:
 *
 * @self: The #CmdlineHandler object
 * @kparams: A %NULL terminated list of strings representing the parameters
 *           to be removed.
 *
 * Removes specific parameter assigments from the cmdline, for example, removing
 * "param=value param=value2" from "param=value param=value2 param" would result
 * in the cmdline "param".
 *
 * Returns: Whether the call was succesful or not
 */
bls_bool      bls_cmdline_remove (BLSCmdline self, const char* kparams[]);

/**
 * bls_cmdline_clear:
 *
 * @self: The #CmdlineHandler object
 * @kparams: A %NULL terminated list of parameter names to be cleared.
 *
 * Removes all instances of the parameters listed in @kparams.
 *
 * Returns: Whether the call was succesful or not
 */
bls_bool      bls_cmdline_clear  (BLSCmdline self, const char* kparams[]);
